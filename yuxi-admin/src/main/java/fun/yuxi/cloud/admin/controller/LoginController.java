package fun.yuxi.cloud.admin.controller;

import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import fun.yuxi.cloud.admin.response.Massage;
import fun.yuxi.common.knife4j.annotation.IgnoreToken;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 登录控制器
 *
 * @author 剧终
 * @version V1.0
 * @date 2021年08月05日 11:58
 */
@RestController
@Api(tags = "登录控制器")
@RequestMapping("/api/auth")
public class LoginController {

    @IgnoreToken
    @GetMapping("/login")
    public Massage<SaTokenInfo> login() {
        StpUtil.login(1);
        return Massage.ok(StpUtil.getTokenInfo());
    }

    @IgnoreToken
    @GetMapping("/logout")
    public String logout() {
        StpUtil.logout();
        return "退出登录";
    }
}
