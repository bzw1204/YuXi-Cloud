package fun.yuxi.cloud.admin.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import fun.yuxi.common.knife4j.annotation.IgnoreToken;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试控制器
 *
 * @author 剧终
 * @version V1.0
 * @date 2021年08月05日 12:00
 */
@RestController
@Api(tags = "测试控制器")
@RequestMapping("/api/test")
public class TestController {

    @IgnoreToken
    @GetMapping("/message")
    public String test1() {
        return "测试1";
    }

    @IgnoreToken
    @SaCheckPermission("101222")
    @GetMapping("/message1")
    public String test2() {
        return "测试2";
    }
}
