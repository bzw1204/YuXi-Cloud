package fun.yuxi.cloud.admin.properties;

import cn.hutool.core.util.ArrayUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 常规配置文件
 *
 * @author 剧终
 * @version V1.0
 * @date 2021年08月25日 10:30
 */
@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "spring.yuxi")
public class YuXiProperties {

    /**
     * 需要忽略认证的接口
     */
    private String[] ignoreList;

    public String[] getIgnoreList() {
        String[] defaultList = new String[]{
                "/swagger-ui.html",
                "/v2/api-docs",
                "/v3/api-docs",
                "/swagger-resources/**",
                "/favicon.ico",
                "/webjars/**",
                "/doc.html",
                "/error",
                "/*.js.map",
                "/api/auth/**",
                "/api/test/message",
        };
        if (ArrayUtil.isNotEmpty(ignoreList)) {
            ArrayUtil.append(ignoreList, defaultList);
        }
        return defaultList;
    }
}
