package fun.yuxi.common.banner.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Swagger自定义配置
 *
 * @author 剧终
 * @version V1.0
 * @date 2022年02月17日 9:22
 */
@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "spring.yuxi.banner")
public class YuXiBannerProperties {

    /**
     * 是否开启 banner 打印
     */
    private boolean enabled;

    /**
     * 应用中文名称
     */
    private String applicationName;


}
