package fun.yuxi.common.banner.runner;

import cn.hutool.core.net.NetUtil;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Banner信息打印
 *
 * @author baizw
 * @version V1.0
 * @date 2021年04月15日 17:10
 */
@Slf4j
@Component
@Order(-1)
@ConditionalOnProperty(value = "spring.yuxi.banner", havingValue = "true", matchIfMissing = true)
public class BannerRunner implements ApplicationRunner {

    private static final String DOC_SUFFIX = "/doc.html";
    @Value("${server.servlet.context-path:}")
    private String contextPath;
    @Value("${spring.yuxi.banner.application-name:spring.application.name:}")
    private String applicationName;
    @Value("${server.port:8080}")
    private int port;

    @Override
    public void run(ApplicationArguments args) {
        try {
            NetUtil.getLocalhostStr();
            String ip = NetUtil.getLocalhostStr();
            String doc = StrUtil.format("文档地址: http://{}:{}{}{}", ip, port, contextPath, DOC_SUFFIX);
            String line = StrUtil.fillAfter(StrUtil.DASHED, '-', doc.length() + 15);
            log.info("\n" +
                    "     )         )     \n" +
                    "  ( /(      ( /(     \n" +
                    "  )\\())  (  )\\())(   \n" +
                    " ((_)\\  ))\\((_)\\ )\\  \n" +
                    "__ ((_)/((_)_((_|(_) \n" +
                    "\\ \\ / (_))(\\ \\/ /(_) \n" +
                    " \\ V /| || |>  < | | \n" +
                    "  |_|  \\_,_/_/\\_\\|_| \n" +
                    "{}服务已启动\n{}\n{}\n", applicationName, doc, line);
        } catch (Exception exception) {
            log.error("为获取到网卡信息", exception);
        }
    }
}
