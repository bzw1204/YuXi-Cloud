package fun.yuxi.common.knife4j.properties;

import cn.hutool.core.util.ArrayUtil;
import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Swagger自定义配置
 *
 * @author 剧终
 * @version V1.0
 * @date 2022年02月17日 9:22
 */
@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "spring.yuxi.swagger")
public class YuXiSwaggerProperties {

    /**
     * 是否开启swagger
     */
    private Boolean enabled;

    /**
     * 需要携带授权的Token名称
     */
    private String tokenName;

    /**
     * swagger需要扫描解析的包路径
     * 不填则默认使用注解Api.class 进行扫描解析
     **/
    private String basePackage = "";

    /**
     * 需要排除授权的接口
     **/
    private String[] ignoreAuthUrls;

    /**
     * 需要扫描的接口
     */
    private String[] includeUrls;

    /**
     * 需要排除扫描的接口
     */
    private String[] excludeUrls;

    /**
     * 分组名称
     */
    private String groupName = "default";

    /**
     * 标题
     **/
    private String title = "";

    /**
     * 描述
     **/
    private String description = "";

    /**
     * 版本
     **/
    private String version = "";

    /**
     * 许可证
     **/
    private String license = "";

    /**
     * 许可证URL
     **/
    private String licenseUrl = "";

    /**
     * 服务条款URL
     **/
    private String termsOfServiceUrl = "";

    /**
     * host信息
     **/
    private String host = "";

    /**
     * 联系人信息
     */
    private Contact contact = new Contact();

    public String[] getExcludeUrls() {
        if (ArrayUtil.isEmpty(excludeUrls)) {
            // 默认的排除路径，排除Spring Boot默认的错误处理路径和端点
            excludeUrls = new String[]{"/error", "/actuator/**"};
        }
        return excludeUrls;
    }

    @Getter
    @Setter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Contact {
        /**
         * 联系人
         **/
        private String name = "";
        /**
         * 联系人url
         **/
        private String url = "";
        /**
         * 联系人email
         **/
        private String email = "";

        public springfox.documentation.service.Contact create() {
            return new springfox.documentation.service.Contact(name, url, email);
        }
    }

}
