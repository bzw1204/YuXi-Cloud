package fun.yuxi.core.exception;

import fun.yuxi.core.response.ResultEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 基础业务异常类
 *
 * @author 剧终
 * @version V1.0
 * @date 2021年02月24日 16:38
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GlobalException extends RuntimeException {

    private static final long serialVersionUID = 3877008832875938912L;

    protected int errorCode;
    protected Object data;

    public GlobalException(String message) {
        super(message);
    }

    public GlobalException(String message, Throwable cause) {
        super(message, cause);
    }

    public GlobalException(int errorCode, String message) {
        this(message);
        this.errorCode = errorCode;
    }

    public GlobalException(int errorCode, String message, Throwable cause) {
        this(message, cause);
        this.errorCode = errorCode;
    }

    public GlobalException(int errorCode, String message, Object data) {
        this(errorCode, message);
        this.data = data;
    }

    public GlobalException(int errorCode, String message, Object data, Throwable cause) {
        this(errorCode, message, cause);
        this.data = data;
    }

    public GlobalException(ResultEnum result) {
        this(result.getCode(), result.getName());
    }

    public GlobalException(ResultEnum result, Throwable cause) {
        this(result.getCode(), result.getName(), cause);
    }

    public GlobalException(ResultEnum result, Object data, Throwable cause) {
        this(result.getCode(), result.getName(), data, cause);
    }
}
