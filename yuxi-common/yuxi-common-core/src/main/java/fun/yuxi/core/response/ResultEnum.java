package fun.yuxi.core.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 消息结果
 *
 * @author 剧终
 * @version V1.0
 * @date 2021年02月24日 16:30
 */
@Getter
@AllArgsConstructor
public enum ResultEnum {

    /**
     * 成功/失败
     */
    SUCCESS(1, "成功"),
    /**
     * 小程序失败，有提示
     */
    FAILURE(0, "失败"),

    /**
     * 小程序失败无错误消息提示
     */
    BIZ_FAILURE(2, "失败"),
    UNAUTHORIZED(403, "抱歉，您没有访问权限"),
    ;

    private final int code;
    private final String name;

}
