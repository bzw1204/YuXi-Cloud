package fun.yuxi.gateway.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 自定义Swagger配置
 *
 * @author 剧终
 * @version V1.0
 * @date 2022年02月17日 10:51
 */
@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "spring.yuxi.swagger")
public class SwaggerProperties {

    /**
     * 是否启动Swagger
     */
    private boolean enable;

    /**
     * 默认分组名称
     */
    private String defaultGroupName = "default";

}
