package fun.yuxi.gateway.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Nacos 动态路由配置
 *
 * @author 剧终
 * @version V1.0
 * @date 2021年09月03日 12:24
 */
@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "spring.yuxi.nacos")
public class NacosDynamicRouteProperties {

    /**
     * 是否开启动态路由
     */
    private boolean enable;

    /**
     * Nacos配置服务地址
     */
    private String serverAddr = "127.0.0.1:8848";

    /**
     * 需要监听的配置文件id
     */
    private String dataId = "gateway-routers";

    /**
     * 需要监听的配置文件分组id
     */
    private String groupId = "DEFAULT_GROUP";
}
