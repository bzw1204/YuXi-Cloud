package fun.yuxi.gateway.config;

import fun.yuxi.gateway.properties.NacosDynamicRouteProperties;
import fun.yuxi.gateway.router.NacosRouteDefinitionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 动态路由配置
 *
 * @author 剧终
 * @version V1.0
 * @date 2021年09月03日 12:51
 */
@Configuration
@RequiredArgsConstructor
@ConditionalOnProperty(prefix = "spring.yuxi.nacos", name = "enabled", havingValue = "true", matchIfMissing = true)
public class DynamicRouteConfig {

    private final ApplicationEventPublisher publisher;
    private final NacosDynamicRouteProperties dynamicRouteProperties;

    @Bean
    public NacosRouteDefinitionRepository nacosRouteDefinitionRepository() {
        return new NacosRouteDefinitionRepository(publisher, dynamicRouteProperties);
    }
}