package fun.yuxi.gateway;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 路由网关入口
 *
 * @author 剧终
 * @version V1.0
 * @date 2021年08月25日 11:37
 */
@Slf4j
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"cn.hutool.extra.spring", "fun.yuxi.gateway","fun.yuxi.common"})
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }

}
